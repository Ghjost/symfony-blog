<?php

namespace App\Event\EntityListener;

use App\Entity\Article;
use DateTime;
use DateTimeImmutable;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class ArticleListener
{
    public function __construct(
        private SluggerInterface $slugger,
        private Security $security
    ) {
    }

    public function prePersist(Article $article)
    {
        $this->slug($article);
    }

    public function preUpdate(Article $article)
    {
        $this->slug($article);
        $article->setUpdatedAt(new DateTimeImmutable());
    }

    public function slug(Article $article)
    {
        $article->setSlug($this->slugger->slug($article->getTitle(), '_'));
    }
}
