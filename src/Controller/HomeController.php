<?php

namespace App\Controller;

use App\Repository\TagRepository;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    public function __construct(
        private ArticleRepository $articleRepository,
        private TagRepository $tagRepository
    ) {
    }

    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        $articles = $this->articleRepository->findBy(['isPublished' => true], ['id' => 'DESC'], 10);
        $tags = $this->getArticleTags($articles);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'articles' => $articles,
            'tags' => $tags
        ]);
    }



    public function getArticleTags(array $articles)
    {
        $tagsList = [];
        foreach ($articles as $key => $article) {
            /**
             * @var Article $article
             */
            foreach ($article->getTags() as $key => $tag) {
                # code...
                if (!in_array($tag, $tagsList)) {
                    $tagsList[] = $tag;
                }
                dump($tag);
            }
        }
        return $tagsList;
    }
}
