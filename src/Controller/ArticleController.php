<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Entity\Commentaire;
use App\Entity\Tag;
use App\Form\CommentaireType;
use App\Security\Voter\ArticleVoter;
use App\Repository\ArticleRepository;
use App\Security\Voter\CommentaireVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/article')]
class ArticleController extends AbstractController
{
    #[Route('/', name: 'app_article_index', methods: ['GET'])]
    public function index(ArticleRepository $articleRepository): Response
    {
        $this->denyAccessUnlessGranted('POST_VIEW_CRUD', new Article());
        return $this->render('article/index.html.twig', [
            'articles' => $articleRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_article_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ArticleRepository $articleRepository): Response
    {
        $article = new Article();
        $this->denyAccessUnlessGranted('POST_NEW', $article);
        $article->setCreatedBy($this->getUser());
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleRepository->save($article, true);

            return $this->redirectToRoute('app_article_show', ['slug' => $article->getSlug()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/new.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }

    #[Route('/{slug}', name: 'app_article_show', methods: ['GET'])]
    public function show(Article $article, Request $request, ArticleRepository $articleRepository): Response
    {
        // $commentaire = new Commentaire();
        // $this->denyAccessUnlessGranted(CommentaireVoter::NEW, $commentaire, 'dddddd');
        // $commentaire->setUser($this->getUser());
        // $form = $this->createForm(CommentaireType::class, $commentaire);
        // $article->addCommentaire($commentaire);
        // $form->handleRequest($request);


        // if ($form->isSubmitted() && $form->isValid()) {
        //     $articleRepository->save($article, true);

        //     return $this->redirectToRoute('app_article_show', ['slug' => $article->getSlug()], Response::HTTP_SEE_OTHER);
        // }

        foreach ($article->getTags() as $key => $value) {
            # code...
            dump($value);
        }

        $this->denyAccessUnlessGranted(ArticleVoter::VIEW, $article, 'article');
        return $this->render('article/show.html.twig', [
            'article' => $article,
            // 'form' => $form->createView(),
        ]);
    }

    #[Route('/{slug}/edit', name: 'app_article_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Article $article, ArticleRepository $articleRepository, EntityManagerInterface $emi): Response
    {

        // foreach ($article->getTags() as $key => $value) {
        //     # code...
        //     dump($value);
        // }

        $this->denyAccessUnlessGranted('POST_EDIT', $article);
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // dd($request);
            $articleRepository->save($article, true);
            // dd($article);

            return $this->redirectToRoute('app_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/edit.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }

    #[Route('/{slug}', name: 'app_article_delete', methods: ['POST'])]
    public function delete(Request $request, Article $article, ArticleRepository $articleRepository): Response
    {
        $this->denyAccessUnlessGranted('POST_DELETE', $article);
        if ($this->isCsrfTokenValid('delete' . $article->getSlug(), $request->request->get('_token'))) {
            $articleRepository->remove($article, true);
        }

        return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
    }
}
