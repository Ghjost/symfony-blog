<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Article;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class ArticleFixtures extends Fixture
{

    // private  SluggerInterface $slug;
    // private EntityManagerInterface $emi;

    public function __construct(
        public SluggerInterface $slug,
        public EntityManagerInterface $emi
    ) {
    }
    public function load(ObjectManager $manager,): void
    {

        $faker = Factory::create();

        for ($i = 0; $i < 15; $i++) {
            $article = new Article();
            $article->setTitle($faker->sentence())
                ->setSlug($this->slug->slug($article->getTitle(), '_'))
                ->setContent($faker->text(100))
                ->setIsPublished($faker->boolean());

            $this->emi->persist($article);
        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
