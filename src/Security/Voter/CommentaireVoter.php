<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CommentaireVoter extends Voter
{
    public const VIEW_CRUD = 'VIEW_CRUD';
    public const NEW = 'POST_NEW';
    public const DELETE = 'POST_DELETE';

    public function __construct(
        private Security $security
    ) {
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::VIEW_CRUD,
            self::NEW,
            self::DELETE
        ])
            && $subject instanceof \App\Entity\Commentaire;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access


        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::VIEW_CRUD:
                return $this->canViewCrud($subject, $user);
                break;
            case self::NEW:
                return $this->canAddNew($subject, $user);
                break;
            case self::DELETE:
                return $this->canDelete($subject, $user);
                break;
        }

        return false;
    }

    public function canViewCrud($subject, $user)
    {
        if ($this->security->isGranted('ROLE_MODERATOR')) {
            return true;
        } else {
            return false;
        }
    }

    public function canAddNew($subject, $user)
    {
        if ($this->security->isGranted('ROLE_SIMPLE_USER')) {
            return true;
        } else {
            return false;
        }
    }

    public function canDelete($subject, $user)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        } else {
            return false;
        }
    }
}
