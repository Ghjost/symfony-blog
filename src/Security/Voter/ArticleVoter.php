<?php

namespace App\Security\Voter;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ArticleVoter extends Voter
{
    public const NEW = 'POST_NEW';
    public const EDIT = 'POST_EDIT';
    public const DELETE = 'POST_DELETE';
    public const VIEW = 'POST_VIEW';
    public const VIEW_CRUD = 'POST_VIEW_CRUD';

    public function __construct(
        private Security $security
    ) {
    }

    protected function supports(string $attribute, $subject): bool
    {

        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::EDIT,
            self::DELETE,
            self::NEW,
            self::VIEW,
            self::VIEW_CRUD
        ])
            && ($subject instanceof Article);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        // if (!$user instanceof UserInterface) {
        //     return false;
        // }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::NEW:
                if ($this->security->isGranted('ROLE_SIMPLE_USER')) {
                    return true;
                } else {
                    dd('oui');
                    return false;
                }
                break;
            case self::EDIT:
                if ($this->security->isGranted('ROLE_MODERATOR') || $subject->getCreatedBy() == $user) {
                    return true;
                } else {
                    return false;
                }
                break;
            case self::VIEW:
                return $this->canView($subject, $user);
                break;
            case self::DELETE:
                if ($this->security->isGranted('ROLE_ADMIN') || $subject->getCreatedBy() == $user) {
                    return true;
                } else {
                    return false;
                }
                break;
            case self::VIEW_CRUD:
                if ($this->security->isGranted('ROLE_MODERATOR')) {
                    return true;
                } else {
                    return false;
                }
                break;
        }

        return false;
    }

    public function canView($subject, $user)
    {
        if ($subject->isIsPublished() == true) {
            return true;
        }
        if ($subject->getCreatedBy() == $user || $this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }
        return false;
    }
}
