<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class TagVoter extends Voter
{
    public const NEW = 'POST_NEW';
    public const EDIT = 'POST_EDIT';
    public const DELETE = 'POST_DELETE';
    public const VIEW = 'POST_VIEW';
    public const VIEW_CRUD = 'POST_VIEW_CRUD';

    public function __construct(
        private Security $security
    ) {
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [
            self::NEW,
            self::EDIT,
            self::DELETE,
            self::VIEW,
            self::VIEW_CRUD,
        ])
            && $subject instanceof \App\Entity\Tag;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::NEW:
                return $this->canCreateTag();
                break;
            case self::EDIT:
                return $this->canEditTag();
                break;
            case self::DELETE:
                return $this->canDeleteTag();
                break;
            case self::VIEW:
                // PUBLIC ACCESS
                return true;
                break;
            case self::VIEW_CRUD:
                // PUBLIC ACCESS
                return true;
                break;
        }

        return false;
    }

    public function canCreateTag()
    {
        if ($this->security->isGranted('ROLE_MODERATOR')) {
            return true;
        }
        return false;
    }

    public function canEditTag()
    {
        if ($this->security->isGranted('ROLE_MODERATOR')) {
            return true;
        }
        return false;
    }

    public function canDeleteTag()
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }
        return false;
    }
}
